import { combineReducers } from 'redux'
import {Map} from 'immutable'

import {
  SIGNUP_SET_NAME,
  SIGNUP_SET_EMAIL,
  SIGNUP_SET_PASSWORD,
  SIGNUP_REQUEST,
  SIGNUP_REQUEST_FAILED,
  SIGNUP_SUCCEEDED,
  SIGNUP_FAILED
} from '../actions/signup'
import {
  LOGIN_SET_EMAIL,
  LOGIN_SET_PASSWORD,
  LOGIN_REQUEST,
  LOGIN_REQUEST_FAILED,
  LOGIN_SUCCEEDED,
  LOGIN_FAILED,
  LOGOUT_SUCCEEDED,
  GET_USER_INFO_SUCCEEDED,
  LOGOUT_REQUEST,
  // LOGOUT_FAILED
} from '../actions/session'
import {
  ADD_IDEA,
  CANCEL_ADD_IDEA,
  NEW_IDEA_CHANGE,
  IDEA_CHANGE,
  EDIT_IDEA,
  CANCEL_EDIT_IDEA,
  CREATE_IDEA_REQUEST,
  CREATE_IDEA_REQUEST_FAILED,
  CREATE_IDEA_SUCCEEDED,
  CREATE_IDEA_FAILED,
  UPDATE_IDEA_REQUEST,
  UPDATE_IDEA_REQUEST_FAILED,
  UPDATE_IDEA_SUCCEEDED,
  UPDATE_IDEA_FAILED,
  IDEA_CONFIRM_DELETE,
  IDEA_DELETE_CANCEL,
  DELETE_IDEA_SUCCEEDED,
  DELETE_IDEA_FAILED,
  FETCH_IDEAS_SUCCEEDED,
  FETCH_IDEAS_FAILED,
  FETCH_IDEAS_REQUEST
} from '../actions/ideas'
import * as ideasApi from '../domain/idea'

/*
 * initial state
 */

const initialState = {
  session: Map({
    token: null,
    user: null,
    login: Map({
      data: Map({
        email: '',
        password: ''
      }),
      validationErrors: Map({
        email: '',
        password: '',
        _error: ''
      }),
      submitting: false
    })
  }),
  ideas: Map({
    add: null,
    delete: null,
    editting: null,
    fetching: false,
    error: '',
    all: Map({}),
    scoreRange: ideasApi.scoreRange()
  }),
  signup: Map({
    data: Map({
      name: '',
      email: '',
      password: ''
    }),
    validationErrors: Map({
      name: '',
      email: '',
      password: '',
      _error: ''
    }),
    submitting: false
  })
}

/*
 * reducers
 */

function signup(state = initialState.signup, action) {
  switch (action.type) {
    case SIGNUP_SET_NAME:
      return state.setIn(['data', 'name'], action.name)
                  .setIn(['validationErrors', 'name'], '');
    case SIGNUP_SET_EMAIL:
      return state.setIn(['data', 'email'], action.email)
                  .setIn(['validationErrors', 'email'], '');
    case SIGNUP_SET_PASSWORD:
      return state.setIn(['data', 'password'], action.password)
                  .setIn(['validationErrors', 'password'], '');
    case SIGNUP_REQUEST:
      return state.set('submitting', true)
    case SIGNUP_REQUEST_FAILED:
      return state.set('validationErrors', action.validation)
    case SIGNUP_SUCCEEDED:
      return state.set('submitting', false)
    case SIGNUP_FAILED:
      return state.set('submitting', false)
                  .setIn(['validationErrors', '_error'], action.error);
    default:
      return state
  }
}

function session(state = initialState.session, action) {
  switch (action.type) {
    case LOGIN_SET_EMAIL:
      return state.setIn(['login', 'data', 'email'], action.email)
                  .setIn(['validationErrors', 'email'], '');
    case LOGIN_SET_PASSWORD:
      return state.setIn(['login', 'data', 'password'], action.password)
                  .setIn(['validationErrors', 'password'], '');
    case LOGIN_REQUEST:
      return state.setIn(['login', 'submitting'], true)
    case LOGIN_REQUEST_FAILED:
      return state.setIn(['login', 'validationErrors'], action.validation)
    case LOGIN_SUCCEEDED:
      return state.set('token', action.token)
                  .set('user', action.user)
                  .set('login', initialState.session.get('login'))
    case LOGIN_FAILED:
      return state.setIn(['login', 'submitting'], false)
                  .setIn(['login', 'validationErrors', '_error'], action.error);
    case LOGOUT_REQUEST:
      return Map({token: null, user: null, login: state.get('login')})
    case LOGOUT_SUCCEEDED:
      return Map()
    case SIGNUP_SUCCEEDED:
      return state.set('user', action.user).set('token', action.token)
    case GET_USER_INFO_SUCCEEDED:
      return state.set('user', action.user).set('token', action.token)
    default:
      return state
  }
}

function ideas(state = initialState.ideas, action) {
  switch (action.type) {
    case ADD_IDEA:
      return state.set('add', ideasApi.emptyIdea())
    case CANCEL_ADD_IDEA:
      return state.set('add', null)
    case EDIT_IDEA:
      return state.setIn(['all', action.idea.get('id'), 'editting'], true)
    case CANCEL_EDIT_IDEA:
      return state.setIn(['all', action.idea.get('id'), 'editting'], false)
    case IDEA_CONFIRM_DELETE:
      return state.set('delete', action.idea)
    case IDEA_DELETE_CANCEL:
      return state.set('delete', null)
    case DELETE_IDEA_SUCCEEDED:
      return state.set('delete', null).deleteIn(['all', action.idea.get('id')])
    case DELETE_IDEA_FAILED:
      return state.set('error', action.error)
    case NEW_IDEA_CHANGE:
      return state.update('add', add => add.merge(Map(action.idea))
                                           .set('validation', Map({})))
    case IDEA_CHANGE:
      const idea = Map(action.idea)
      return state.updateIn(['all', idea.get('id')],
                            existing => existing.merge(idea))
    case CREATE_IDEA_REQUEST:
      return state
    case UPDATE_IDEA_REQUEST:
      // optimistic update
      return state.setIn(['all', action.idea.get('id'), 'editting'], false)
                  .setIn(['all', action.idea.get('id'), 'average'],
                         ideasApi.calculateAverage(action.idea))
    case UPDATE_IDEA_SUCCEEDED:
      return state.setIn(['all', action.idea.get('id')], action.idea)
    case UPDATE_IDEA_FAILED:
        return state.set('error', action.error)
    case CREATE_IDEA_SUCCEEDED:
      return state.update(
                    'all',
                    all => all.set(action.newIdea.get('id'), action.newIdea))
                  .set('add', null)
    case UPDATE_IDEA_REQUEST_FAILED:
      return state.setIn(
        ['all', action.idea.get('id'), 'validation'],
        action.validation
      )
    case CREATE_IDEA_REQUEST_FAILED:
      return state.setIn(['add', 'validation'], action.validation)
    case CREATE_IDEA_FAILED:
      return state.set('error', action.error)
    case FETCH_IDEAS_REQUEST:
      return state
    case FETCH_IDEAS_SUCCEEDED:
      return state.set('error', '').set('all', action.ideas)
    case FETCH_IDEAS_FAILED:
      return state.set('error', action.error).set('all', action.ideas)
    default:
      return state
  }
}

const ideasState = combineReducers({
  signup,
  session,
  ideas
})

export default ideasState
