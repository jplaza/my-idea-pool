import * as signupActions from '../actions/signup'
import ideasState from './index'

test('signup data should be empty on initial state', () => {
  const state = ideasState(undefined, {})
  expect(state.signup.getIn(['data', 'name'])).toBe('')
  expect(state.signup.getIn(['data', 'email'])).toBe('')
  expect(state.signup.getIn(['data', 'password'])).toBe('')
});

test('signup set name action', () => {
  const state = ideasState(
    undefined,
    {
      type: signupActions.SIGNUP_SET_NAME,
      name: 'Gamora'
    })
  expect(state.signup.getIn(['data', 'name'])).toBe('Gamora')
});

test('signup set email action', () => {
  const state = ideasState(
    undefined,
    {
      type: signupActions.SIGNUP_SET_EMAIL,
      email: 'gamora@marvel.com'
    })
  expect(state.signup.getIn(['data','email'])).toBe('gamora@marvel.com')
});

test('signup set password action', () => {
  const state = ideasState(
    undefined,
    {
      type: signupActions.SIGNUP_SET_PASSWORD,
      password: 'xxxxxxxxxxx'
    })
  expect(state.signup.getIn(['data', 'password'])).toBe('xxxxxxxxxxx')
});
