import { connect } from 'react-redux'
import Sidebar from '../components/Sidebar'
import {
  logout
} from '../actions/session'


const mapStateToProps = (state, ownProps) => {
  return {
    session: state.session,
    loggedIn: state.session.getIn(['token', 'jwt'], '') !== ''
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onLogoutClick: (token) => {
      dispatch(logout(ownProps.service, token))
    }
  }
}

const SidebarContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Sidebar)

export default SidebarContainer
