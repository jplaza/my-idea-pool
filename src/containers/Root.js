import React from 'react';
import { Provider } from 'react-redux';
import {
  BrowserRouter as Router,
  // Route,
  // Redirect
} from 'react-router-dom'
import AppContainer from './AppContainer'


const Root = (props) => {
  return (
    <Provider store={props.store}>
      <Router>
        <AppContainer service={props.service} />
      </Router>
    </Provider>
  );
}

export default Root;
