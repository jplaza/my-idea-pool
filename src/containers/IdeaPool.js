import { connect } from 'react-redux'
import IdeaList from '../components/IdeaList'
import * as ideaApi from '../domain/idea'
import {
  addIdea,
  editIdea,
  cancelAddIdea,
  cancelEditIdea,
  ideaCancelDelete,
  createIdea,
  updateIdea,
  deleteIdea,
  newIdeaChange,
  ideaChange,
  fetchIdeas,
  ideaConfirmDelete
} from '../actions/ideas'


const mapStateToProps = (state, ownProps) => {
  const ideas = state.ideas
  // const errors = login.get('validationErrors')
  return {
    ideas: ideas.get('all'),
    add: ideas.get('add'),
    delete: ideas.get('delete'),
    scoreRange: ideas.get('scoreRange'),
    error: ideas.get('error')
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    fetchIdeas: () => {
      dispatch(fetchIdeas(ownProps.service))
    },
    onAddClick: () => {
      dispatch(addIdea())
    },
    onIdeaEditClick: (idea) => {
      dispatch(editIdea(idea))
    },
    onIdeaDeleteClick: (idea) => {
      dispatch(ideaConfirmDelete(idea))
    },
    onIdeaDeleteCancelClick: (idea) => {
      dispatch(ideaCancelDelete())
    },
    onIdeaConfirmDeleteClick: (idea) => {
      dispatch(deleteIdea(ownProps.service, idea))
    },
    onIdeaSaveClick: (idea) => {
      dispatch(updateIdea(ownProps.service, idea))
    },
    onIdeaEditCancelClick: (idea) => {
      dispatch(cancelEditIdea(idea))
    },
    onNewIdeaSave: (idea) => {
      dispatch(createIdea(ownProps.service, ideaApi.createIdeaReq(idea)))
    },
    onNewIdeaCancel: () => {
      dispatch(cancelAddIdea())
    },
    onNewIdeaChange: (idea) => {
      dispatch(newIdeaChange(idea))
    },
    onIdeaChange: (idea) => {
      dispatch(ideaChange(idea))
    }
  }
}

const IdeaPool = connect(
  mapStateToProps,
  mapDispatchToProps
)(IdeaList)

export default IdeaPool
