import { connect } from 'react-redux'
import App from '../components/App'
import { withRouter } from 'react-router-dom'


const mapStateToProps = (state, ownProps) => {
  return {
    session: state.session,
    isAuthenticated: state.session.getIn(['token', 'jwt'], '') !== ''
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    // onLogoutClick: () => {
    //   dispatch(logout())
    // }
  }
}

const AppContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(App)

export default withRouter(AppContainer)
