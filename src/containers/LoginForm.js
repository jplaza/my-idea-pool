import { Map } from 'immutable'
import { connect } from 'react-redux'
import Login from '../components/Login'
import {
  loginSetEmail,
  loginSetPassword,
  login
} from '../actions/session'


const mapStateToProps = (state, ownProps) => {
  const session = state.session
  const login = state.session.get('login')
  const errors = session.getIn(['login', 'validationErrors']) || Map()
  const concat = (str1, str2) => str1.concat(str2)
  return {
    data: login.get('data'),
    errors: errors,
    submitting: login.get('submitting'),
    valid: errors.valueSeq().reduce(concat, '') === ''
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onLoginClick: (loginReq) => {
      dispatch(login(ownProps.service, loginReq))
    },
    onEmailChange: (newEmail) => {
      dispatch(loginSetEmail(newEmail))
    },
    onPasswordChange: (newPassword) => {
      dispatch(loginSetPassword(newPassword))
    },
  }
}

const LoginForm = connect(
  mapStateToProps,
  mapDispatchToProps
)(Login)

export default LoginForm
