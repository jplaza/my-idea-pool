import { connect } from 'react-redux'
import Signup from '../components/Signup'
import {
  setName,
  setEmail,
  setPassword,
  signup
} from '../actions/signup'


const mapStateToProps = (state, ownProps) => {
  const errors = state.signup.get('validationErrors')
  const concat = (str1, str2) => str1.concat(str2)
  return {
    data: state.signup.get('data'),
    errors: errors,
    submitting: state.signup.get('submitting'),
    valid: errors.valueSeq().reduce(concat, '') === ''
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onSignupClick: (signupReq) => {
      dispatch(signup(ownProps.service, signupReq))
    },
    onNameChange: (newName) => {
      dispatch(setName(newName))
    },
    onEmailChange: (newEmail) => {
      dispatch(setEmail(newEmail))
    },
    onPasswordChange: (newPassword) => {
      dispatch(setPassword(newPassword))
    },
  }
}

const SignupForm = connect(
  mapStateToProps,
  mapDispatchToProps
)(Signup)

export default SignupForm
