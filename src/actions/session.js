import fetch from 'cross-fetch'
import {Map} from 'immutable'
import * as sessionApi from '../domain/session'
import * as env from '../utils/env'

/*
 * action types
 */

export const LOGIN_SET_EMAIL = 'LOGIN_SET_EMAIL'
export const LOGIN_SET_PASSWORD = 'LOGIN_SET_PASSWORD'
export const LOGIN_REQUEST = 'LOGIN_REQUEST'
export const LOGIN_REQUEST_FAILED = 'LOGIN_REQUEST_FAILED'
export const LOGIN_SUCCEEDED = 'LOGIN_SUCCEEDED'
export const LOGIN_FAILED = 'LOGIN_FAILED'

export const LOGOUT_REQUEST = 'LOGOUT_REQUEST'
export const LOGOUT_SUCCEEDED = 'LOGOUT_SUCCEEDED'
export const LOGOUT_FAILED = 'LOGOUT_FAILED'

export const GET_USER_INFO_SUCCEEDED = 'GET_USER_INFO_SUCCEEDED'
export const GET_USER_INFO_FAILED = 'GET_USER_INFO_FAILED'

export const REFRESH_TOKEN_SUCCEEDED = 'REFRESH_TOKEN_SUCCEEDED'

/*
 * other constants
 */


/*
 * action creators
 */

export function loginSetEmail(email) {
  return { type: LOGIN_SET_EMAIL, email }
}

export function loginSetPassword(password) {
  return { type: LOGIN_SET_PASSWORD, password }
}

export function loginRequest() {
  return { type: LOGIN_REQUEST }
}

export function loginRequestFailed(validation) {
  return { type: LOGIN_REQUEST_FAILED, validation }
}

export function loginSuceeded(token, user) {
  return { type: LOGIN_SUCCEEDED, token: Map(token), user: Map(user) }
}

export function loginFailed(error) {
  return { type: LOGIN_FAILED, error }
}

export function logoutRequest() {
  return { type: LOGOUT_REQUEST }
}

export function logoutSuceeded() {
  return { type: LOGOUT_SUCCEEDED }
}

export function logoutFailed(error) {
  return { type: LOGOUT_FAILED, error }
}

export function getUserInfoSucceeded(user, token) {
  return { type: GET_USER_INFO_SUCCEEDED, user: Map(user), token }
}

export function getUserInfoFailed(error) {
  return { type: GET_USER_INFO_FAILED, error }
}

export function refreshTokenSucceeded(token) {
  return { type: REFRESH_TOKEN_SUCCEEDED, token: Map(token) }
}

/*
 * async action creators
 */

/*
 * performs login request
 * @param loginReq Object containing service information, at least url
 * @param loginReq Immutable.Map containing email and password keys
 */

export function login(service, loginReq) {
  return async (dispatch) => {
    const validation = sessionApi.validateLoginRequest(loginReq)
    if (validation.get('valid')) {
      dispatch(loginRequest())
    } else {
      return dispatch(loginRequestFailed(validation.get('validation')))
    }
    try {
      const res = await fetch(
        service.url + "/access-tokens",
        {
          method: "POST",
          body: JSON.stringify(loginReq),
          headers: {
            "Content-Type": "application/json"
          },
          credentials: "same-origin"
        }
      );
      if (res.status >= 500) {
        throw new Error("Sorry something went wrong, please try again");
      }
      const loginResp = await res.json();
      if (res.status >= 400) {
        throw new Error(loginResp.reason);
      }
      const userResp = await fetch(
        service.url + "/me",
        {
          headers: {
            "X-Access-Token": loginResp.jwt
          }
        }
      )
      const user = await userResp.json();
      if (res.status >= 400) {
        throw new Error(userResp.reason);
      }
      env.setCookie('sessiontoken', JSON.stringify(loginResp))
      dispatch(loginSuceeded(loginResp, user))
    } catch (err) {
      dispatch(loginFailed(err.message))
    }
  }
}

/*
 * performs logout request
 * @param loginReq Object containing service information, at least url
 * @param token Object containing jwt and refresh_token keys
 */

export function logout(service, token) {
  return async (dispatch) => {
    env.setCookie('sessiontoken', '')
    dispatch(logoutRequest())
    try {
      const logoutReq = {
        refresh_token: token.get('refresh_token')
      }
      const res = await fetch(
        service.url + "/access-tokens",
        {
          method: "DELETE",
          body: JSON.stringify(logoutReq),
          headers: {
            "Content-Type": "application/json",
            "X-Access-Token": token.get('jwt')
          },
          credentials: "same-origin"
        }
      );
      if (res.status >= 500) {
        throw new Error("Sorry something went wrong, please try again");
      }
      const loginResp = await res.json();
      if (res.status >= 400) {
        throw new Error(loginResp.reason);
      }
      dispatch(logoutSuceeded())
    } catch (err) {
      dispatch(logoutFailed(err.message))
    }
  }
}

/*
 * performs login request
 * @param loginReq Object containing service information, at least url
 * @param loginReq Immutable.Map containing email and password keys
 */

export function getUserInfo(service, token) {
  return async (dispatch) => {
    try {
      let _token = Map(token)
      const userResp = await fetch(
        service.url + "/me",
        {
          headers: {
            "X-Access-Token": token.get('jwt')
          }
        }
      )
      let user = await userResp.json();
      if (userResp.status === 401 && token.get('refresh_token')) {
        const newToken = await refreshSession(service, token)
        _token = _token.set('jwt', newToken.jwt)
        user = await getUser(service, _token)
      } else if (userResp.status >= 400) {
        throw new Error(user.reason);
      }
      env.setCookie('sessiontoken', JSON.stringify(_token.toJS()))
      dispatch(getUserInfoSucceeded(user, _token))
    } catch (err) {
      console.log(err);
      dispatch(getUserInfoFailed(err.message))
    }
  }
}

async function getUser(service, token) {
  const userResp = await fetch(
    service.url + "/me",
    {
      headers: {
        "X-Access-Token": token.get('jwt')
      }
    }
  )
  return userResp.json();
}

async function refreshSession(service, token) {
  try {
    const refreshReq = {
      refresh_token: token.get('refresh_token')
    }
    const refreshResp = await fetch(
      service.url + "/access-tokens/refresh",
      {
        method: "POST",
        body: JSON.stringify(refreshReq),
        headers: {
          "Content-Type": "application/json",
          "X-Access-Token": token.get('jwt')
        }
      }
    )
    const newToken = await refreshResp.json();
    if (refreshResp.status >= 400) {
      throw new Error(newToken.reason);
    }
    return new Promise(resolve => resolve(newToken))
  } catch (err) {
    console.log(err);
    return new Promise(resolve => resolve(err))
  }
}

export function refreshToken(service, token) {
  return async (dispatch) => {
    try {
      const refreshReq = {
        refresh_token: token.get('refresh_token')
      }
      const refreshResp = await fetch(
        service.url + "/access-tokens/refresh",
        {
          method: "POST",
          body: JSON.stringify(refreshReq),
          headers: {
            "X-Access-Token": token.get('jwt')
          }
        }
      )
      const newToken = await refreshResp.json();
      if (refreshResp.status >= 400) {
        throw new Error(newToken.reason);
      }
      dispatch(refreshTokenSucceeded(newToken))
    } catch (err) {
      console.log(err);
      // dispatch(refreshTokenFailed(err.message))
    }
  }
}
