import fetch from 'cross-fetch'
import * as ideaApi from '../domain/idea'
import { Map } from 'immutable'

/*
 * action types
 */

export const ADD_IDEA = 'ADD_IDEA'
export const CANCEL_ADD_IDEA = 'CANCEL_ADD_IDEA'
export const NEW_IDEA_CHANGE = 'NEW_IDEA_CHANGE'
export const IDEA_CHANGE = 'IDEA_CHANGE'
export const EDIT_IDEA = 'EDIT_IDEA'
export const CANCEL_EDIT_IDEA = 'CANCEL_EDIT_IDEA'

export const CREATE_IDEA_REQUEST = 'CREATE_IDEA_REQUEST'
export const CREATE_IDEA_REQUEST_FAILED = 'CREATE_IDEA_REQUEST_FAILED'
export const CREATE_IDEA_SUCCEEDED = 'CREATE_IDEA_SUCCEEDED'
export const CREATE_IDEA_FAILED = 'CREATE_IDEA_FAILED'

export const UPDATE_IDEA_REQUEST = 'UPDATE_IDEA_REQUEST'
export const UPDATE_IDEA_REQUEST_FAILED = 'UPDATE_IDEA_REQUEST_FAILED'
export const UPDATE_IDEA_SUCCEEDED = 'UPDATE_IDEA_SUCCEEDED'
export const UPDATE_IDEA_FAILED = 'UPDATE_IDEA_FAILED'

export const IDEA_CONFIRM_DELETE = 'IDEA_CONFIRM_DELETE'
export const IDEA_DELETE_CANCEL = 'IDEA_DELETE_CANCEL'
export const DELETE_IDEA_REQUEST_FAILED = 'DELETE_IDEA_REQUEST_FAILED'
export const DELETE_IDEA_SUCCEEDED = 'DELETE_IDEA_SUCCEEDED'
export const DELETE_IDEA_FAILED = 'DELETE_IDEA_FAILED'

export const FETCH_IDEAS_REQUEST = 'FETCH_IDEAS_REQUEST'
export const FETCH_IDEAS_SUCCEEDED = 'FETCH_IDEAS_SUCCEEDED'
export const FETCH_IDEAS_FAILED = 'CREATE_IDEA_FAILED'

/*
 * other constants
 */


/*
 * action creators
 */


 /*
  * @param idea Map containing "text", "impact", "ease", "confidence" keys
  */

export function addIdea(idea) {
  return { type: ADD_IDEA, idea }
}

export function cancelAddIdea() {
  return { type: CANCEL_ADD_IDEA }
}

export function editIdea(idea) {
  return { type: EDIT_IDEA, idea }
}

export function cancelEditIdea(idea) {
  return { type: CANCEL_EDIT_IDEA, idea }
}


export function createIdeaRequest() {
  return { type: CREATE_IDEA_REQUEST }
}

export function createIdeaRequestFailed(validation) {
  return { type: CREATE_IDEA_REQUEST_FAILED, validation }
}

export function createIdeaSuceeded(newIdea) {
  return { type: CREATE_IDEA_SUCCEEDED, newIdea }
}

export function createIdeaFailed(error) {
  return { type: CREATE_IDEA_FAILED, error }
}

export function updateIdeaRequest(idea) {
  return { type: UPDATE_IDEA_REQUEST, idea }
}

export function updateIdeaRequestFailed(idea, validation) {
  return { type: UPDATE_IDEA_REQUEST_FAILED, idea, validation }
}

export function updateIdeaSuceeded(idea) {
  return { type: UPDATE_IDEA_SUCCEEDED, idea: serviceIdeaToIdea(idea) }
}

export function updateIdeaFailed(error) {
  return { type: UPDATE_IDEA_FAILED, error }
}

export function ideaConfirmDelete(idea) {
  return { type: IDEA_CONFIRM_DELETE, idea }
}

export function ideaCancelDelete() {
  return { type: IDEA_DELETE_CANCEL }
}

export function deleteIdeaRequestFailed(validation) {
  return { type: DELETE_IDEA_REQUEST_FAILED, validation }
}

export function deleteIdeaSuceeded(idea) {
  return { type: DELETE_IDEA_SUCCEEDED, idea }
}

export function deleteIdeaFailed(error) {
  return { type: DELETE_IDEA_FAILED, error }
}

export function newIdeaChange(idea) {
  return { type: NEW_IDEA_CHANGE, idea }
}

export function ideaChange(idea) {
  return { type: IDEA_CHANGE, idea }
}

function serviceIdeaToIdea(serviceIdea) {
  return Map({
    id: serviceIdea.id,
    content: serviceIdea.content,
    average: serviceIdea.average_score,
    ease: serviceIdea.ease,
    impact: serviceIdea.impact,
    confidence: serviceIdea.confidence,
    created: serviceIdea.created_at
  })
}

export function fetchIdeasSuceeded(ideas) {
  const ideasMap = Map(ideas.map(idea => [idea.id, serviceIdeaToIdea(idea)]))
  return { type: FETCH_IDEAS_SUCCEEDED, ideas: ideasMap }
}

export function fetchIdeasRequest() {
  return { type: FETCH_IDEAS_REQUEST }
}

export function fetchIdeasFailed(error) {
  return { type: FETCH_IDEAS_FAILED, error }
}

/*
 * async action creators
 */

/*
 * performs create Idea request
 * @param ideaReq Object contining service information, at least url
 * @param ideaReq Immutable.Map containing email and password keys
 */

export function createIdea(service, ideaReq) {
  return async (dispatch, getState) => {
    const validation = ideaApi.validateCreateRequest(ideaReq)
    const session = getState().session
    console.log(validation);
    if (validation.get('valid')) {
      dispatch(createIdeaRequest())
    } else {
      return dispatch(createIdeaRequestFailed(validation.get('validation')))
    }
    try {
      const res = await fetch(
        service.url + "/ideas",
        {
          method: "POST",
          body: JSON.stringify(ideaReq.toJS()),
          headers: {
            "Content-Type": "application/json",
            "X-Access-Token": session.getIn(['token', 'jwt'])
          },
          credentials: "same-origin"
        }
      );
      if (res.status >= 500) {
        throw new Error("Sorry something went wrong, please try again");
      }
      const createIdeaResp = await res.json();
      if (res.status >= 400) {
        throw new Error(createIdeaResp.reason);
      }
      dispatch(createIdeaSuceeded(serviceIdeaToIdea(createIdeaResp)))
    } catch (err) {
      dispatch(createIdeaFailed(err.message))
    }
  }
}

/*
 * performs create Idea request
 * @param service Object contining service information, at least url
 * @param idea Immutable.Map containing email and password keys
 */

export function updateIdea(service, idea) {
  return async (dispatch, getState) => {
    const validation = ideaApi.validateUpdateRequest(idea)
    if (validation.get('valid')) {
      dispatch(updateIdeaRequest(idea))
    } else {
      return dispatch(updateIdeaRequestFailed(idea, validation.get('validation')))
    }
    try {
      const session = getState().session
      const res = await fetch(
        service.url + "/ideas/" + idea.get('id'),
        {
          method: "PUT",
          body: JSON.stringify(ideaApi.updateIdeaRequest(idea).toJS()),
          headers: {
            "Content-Type": "application/json",
            "X-Access-Token": session.getIn(['token', 'jwt'])
          },
          credentials: "same-origin"
        }
      );
      if (res.status >= 500) {
        throw new Error("Sorry something went wrong, please try again");
      }
      const updateIdeaResp = await res.json();
      if (res.status >= 400) {
        throw new Error(updateIdeaResp.reason);
      }
      dispatch(updateIdeaSuceeded(updateIdeaResp))
    } catch (err) {
      console.log(err);
      dispatch(updateIdeaFailed(err.message))
    }
  }
}
/*
 * performs create Idea request
 * @param ideaReq Object contining service information, at least url
 * @param ideaReq Immutable.Map containing email and password keys
 */

export function deleteIdea(service, idea) {
  return async (dispatch, getState) => {
    try {
      const session = getState().session
      const res = await fetch(
        `${service.url}/ideas/${idea.get('id')}`,
        {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
            "X-Access-Token": session.getIn(['token', 'jwt'])
          },
          credentials: "same-origin"
        }
      );
      if (res.status >= 500) {
        throw new Error("Sorry something went wrong, please try again");
      }
      if (res.status >= 400) {
        const deleteIdeaResp = await res.json();
        throw new Error(deleteIdeaResp.reason);
      }
      dispatch(deleteIdeaSuceeded(idea))
    } catch (err) {
      console.log(err);
      dispatch(deleteIdeaFailed(err.message))
    }
  }
}

/*
 * performs create Idea request
 * @param ideaReq Object contining service information, at least url
 * @param ideaReq Immutable.Map containing email and password keys
 */

export function fetchIdeas(service) {
  return async (dispatch, getState) => {
    const session = getState().session
    try {
      dispatch(fetchIdeasRequest())
      const res = await fetch(
        service.url + "/ideas",
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            "X-Access-Token": session.getIn(['token', 'jwt'])
          },
          credentials: "same-origin"
        }
      );
      if (res.status >= 500) {
        throw new Error("Sorry something went wrong, please try again");
      }
      const fetchIdeasResp = await res.json();
      if (res.status >= 400) {
        throw new Error(fetchIdeasResp.reason);
      }
      dispatch(fetchIdeasSuceeded(fetchIdeasResp))
    } catch (err) {
      console.log(err);
      dispatch(fetchIdeasFailed(err.message))
    }
  }
}