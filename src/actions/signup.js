import fetch from 'cross-fetch'
import { Map } from 'immutable'
import * as signupApi from '../domain/signup'
import * as env from '../utils/env'

/*
 * action types
 */

export const SIGNUP_SET_NAME = 'SIGNUP_SET_NAME'
export const SIGNUP_SET_EMAIL = 'SIGNUP_SET_EMAIL'
export const SIGNUP_SET_PASSWORD = 'SIGNUP_SET_PASSWORD'
export const SIGNUP_REQUEST = 'SIGNUP_REQUEST'
export const SIGNUP_REQUEST_FAILED = 'SIGNUP_REQUEST_FAILED'
export const SIGNUP_SUCCEEDED = 'SIGNUP_SUCCEEDED'
export const SIGNUP_FAILED = 'SIGNUP_FAILED'

/*
 * other constants
 */


/*
 * action creators
 */

export function setName(name) {
  return { type: SIGNUP_SET_NAME, name }
}

export function setEmail(email) {
  return { type: SIGNUP_SET_EMAIL, email }
}

export function setPassword(password) {
  return { type: SIGNUP_SET_PASSWORD, password }
}

export function signupRequest() {
  return { type: SIGNUP_REQUEST }
}

export function signupRequestFailed(validation) {
  return { type: SIGNUP_REQUEST_FAILED, validation }
}

export function signupSuceeded(token, user) {
  return { type: SIGNUP_SUCCEEDED, token: Map(token), user: Map(user) }
}

export function signupFailed(error) {
  return { type: SIGNUP_FAILED, error }
}

/*
 * async action creators
 */

export function signup(service, signupReq) {
  return async (dispatch) => {
    const validation = signupApi.validateRequest(signupReq)
    if (validation.get('valid')) {
      dispatch(signupRequest())
    } else {
      return dispatch(signupRequestFailed(validation.get('validation')))
    }
    try {
      const res = await fetch(
        service.url + "/users",
        {
          method: "POST",
          body: JSON.stringify(signupReq.toJS()),
          headers: {
            "Content-Type": "application/json"
          },
          credentials: "same-origin"
        }
      );
      if (res.status >= 500) {
        throw new Error("Sorry something went wrong, please try again");
      }
      const signupResp = await res.json();
      if (res.status >= 400) {
        throw new Error(signupResp.reason);
      }
      const userResp = await fetch(
        service.url + "/me",
        {
          headers: {
            "X-Access-Token": signupResp.jwt
          }
        }
      )
      const user = await userResp.json();
      if (res.status >= 400) {
        throw new Error(userResp.reason);
      }
      env.setCookie('sessiontoken', JSON.stringify(signupResp))
      dispatch(signupSuceeded(signupResp, user))
    } catch (err) {
      dispatch(signupFailed(err.message))
    }
  }
}
