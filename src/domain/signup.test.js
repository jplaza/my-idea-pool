import {validatePassword, INVALID_PASSWORD_MESSAGE} from './signup';


test('password validation with invalid password', () => {
  const invalidPassword = '1234'
  expect(validatePassword(invalidPassword)).toEqual({
    valid: false,
    message: INVALID_PASSWORD_MESSAGE
  })
});

test('password validation with valid password', () => {
  const invalidPassword = 'thisISva1id'
  expect(validatePassword(invalidPassword)).toEqual({
    valid: true,
    message: null
  })
});

test('password validation with invalid password length', () => {
  const invalidPassword = 'ISntva1'
  expect(validatePassword(invalidPassword)).toEqual({
    valid: false,
    message: INVALID_PASSWORD_MESSAGE
  })
});

test('password validation with null password', () => {
  expect(validatePassword(null)).toEqual({
    valid: false,
    message: INVALID_PASSWORD_MESSAGE
  })
});
