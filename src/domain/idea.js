import {Map, Range} from 'immutable'

export function emptyIdea() {
  return Map({
    content: "",
    impact: 10,
    ease: 10,
    confidence: 10,
    average: 10,
    created: -1,
    editting: true,
    validation: Map()
  })
}

export function scoreRange() {
  return Range(1, 11)
}

export function createIdeaReq(idea) {
  return Map({
    content: idea.get('content'),
    impact: idea.get('impact'),
    ease: idea.get('ease'),
    confidence: idea.get('confidence')
  })
}

export function validateCreateRequest(req) {
  const isValid = Boolean(req.get('content', ''))
  const contentErr = isValid? "":"Write something for your next big idea!"
  return Map({
    valid: isValid,
    validation: Map({
      content: contentErr,
      impact: "",
      ease: "",
      confidence: ""
    })
  })
}

export function validateUpdateRequest(req) {
  const isValid = Boolean(req.get('content', ''))
  const contentErr = isValid? "":"Write something for your big idea!"
  return Map({
    valid: isValid,
    validation: Map({
      content: contentErr,
      impact: "",
      ease: "",
      confidence: ""
    })
  })
}

export function updateIdeaRequest(idea) {
  return Map({
    content: idea.get('content'),
    ease: idea.get('ease'),
    impact: idea.get('impact'),
    confidence: idea.get('confidence')
  })
}

export function calculateAverage(idea) {
  return (idea.get('impact') + idea.get('ease') + idea.get('confidence')) / 3
}
