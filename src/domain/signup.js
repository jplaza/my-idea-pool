import {Map} from 'immutable'


export const INVALID_PASSWORD_MESSAGE = 'Password should be at least 8 characters long, include at least one uppercase letter, one lowercase letter, and one number'

export function validatePassword(password) {
  const pass = password || ""
  const hasUppercase = Boolean(pass.match(/[A-Z]+/))
  const hasLowercase = Boolean(pass.match(/[a-z]+/))
  const hasNumbers = Boolean(pass.match(/[0-9]+/))
  const hasExptedLength = (pass || '').length >= 8
  const valid = hasNumbers && hasLowercase && hasUppercase && hasExptedLength
  const message = valid? null:INVALID_PASSWORD_MESSAGE
  return {
    valid,
    message
  }
}

export function validateRequest(data) {
  let nameErr = '', emailErr = '', passwordErr = ''
  if (data.get('name') === '') {
    nameErr = 'This field is required'
  }
  if (data.get('email') === '') {
    emailErr = 'This field is required'
  }
  if (data.get('password', '') === '') {
    passwordErr = 'This field is required'
  }
  const passwordValidation = validatePassword(data.get('password', ''))
  if (!passwordValidation.valid) {
    passwordErr = passwordValidation.message
  }
  return Map({
    validation: Map({
      name: nameErr,
      email: emailErr,
      password: passwordErr
    }),
    valid: !(nameErr || emailErr || passwordErr)
  })
}