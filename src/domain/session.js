import {Map} from 'immutable'


export function validateLoginRequest(data) {
  let emailErr = '', passwordErr = ''
  if (data.get('email') === '') {
    emailErr = 'This field is required'
  }
  if (data.get('password', '') === '') {
    passwordErr = 'This field is required'
  }
  return Map({
    validation: Map({
      email: emailErr,
      password: passwordErr
    }),
    valid: !(emailErr || passwordErr)
  })
}