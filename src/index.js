import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {Map} from 'immutable'
import './index.css';
import Root from './containers/Root';
import ideasAppState from './reducers';
import {getCookie} from './utils/env'
import * as ideasApi from './domain/idea'
import {getUserInfo} from './actions/session'

const service = {
  url: 'https://small-project-api.herokuapp.com'
}

const sessionToken = Map(JSON.parse(getCookie('sessiontoken') || 'null'))
const initialState = {
  session: Map({
    token: sessionToken,
    user: null,
    login: Map({
      data: Map({email: '', password: ''}),
      validationErrors: Map({email: '', password: '', _error: ''}),
      submitting: false
    })
  }),
  ideas: Map({
    add: null,
    editting: null,
    fetching: false,
    error: '',
    all: Map({}),
    scoreRange: ideasApi.scoreRange()
  }),
  signup: Map({
    data: Map({name: '', email: '', password: ''}),
    validationErrors: Map({name: '', email: '', password: '', _error: ''}),
    submitting: false
  })
}

const store = createStore(
  ideasAppState,
  initialState,
  applyMiddleware(thunk)
)
if (sessionToken.size) {
  store.dispatch(getUserInfo(service, sessionToken))
}

ReactDOM.render(<Root service={service} store={store}/>, document.getElementById('root'))
