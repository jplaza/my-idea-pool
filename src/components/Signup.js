import React, { Component } from 'react';
import Button from './Button';
import TextField from './TextField';
import './Signup.css';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import Alert from './Alert';
import { NavLink } from 'react-router-dom'


class Signup extends Component {
  constructor(props) {
    super(props)
    this.handleSignupClick = this.handleSignupClick.bind(this)
  }
  handleSignupClick(evt) {
    this.props.onSignupClick(this.props.data)
  }
  buttonText() {
    return this.props.submitting? 'Sending...':'Sign up'
  }
  render() {
    const props = this.props
    const data = this.props.data
    const errorMessage = (msg) => ({
      text: msg,
      type: 'error'
    })
    return (
      <div className="Signup">
        <h1>Sign Up</h1>
        <Alert type="error" message={props.errors.get('_error')} />
        <form>
          <TextField
            placeholder="Name"
            type="text"
            value={data.get('name') || ''}
            onChange={e => this.props.onNameChange(e.target.value)}
            message={errorMessage(props.errors.get('name'))} />
          <TextField
            placeholder="Email"
            type="text"
            value={data.get('email') || ''}
            onChange={e => this.props.onEmailChange(e.target.value)}
            message={errorMessage(props.errors.get('email'))} />
          <TextField
            placeholder="Password"
            type="password"
            value={data.get('password') || ''}
            onChange={e => this.props.onPasswordChange(e.target.value)}
            message={errorMessage(props.errors.get('password'))} />
          <div className="Signup-form-footer">
            <Button
                btnStyle="primary"
                disable={this.props.submitting}
                onClick={this.handleSignupClick}>
              { this.buttonText() }
            </Button>
            <div className="Signup-login-link">
              Already have an account? <NavLink to="/login">Log in</NavLink>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

Signup.propTypes = {
  onSignupClick: PropTypes.func.isRequired,
  onNameChange: PropTypes.func.isRequired,
  onEmailChange: PropTypes.func.isRequired,
  onPasswordChange: PropTypes.func.isRequired,
  data: ImmutablePropTypes.contains({
    name: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired
  }).isRequired,
  errors: ImmutablePropTypes.contains({
    name: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired
  }).isRequired
}

export default Signup;
