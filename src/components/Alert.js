import React from 'react';
import './Alert.css';

const Alert = ({type, message}) => (
  <div className={"Alert Alert-" + type + " " + (message? "":"hidden")}>
    {message}
  </div>
)

export default Alert