import React from 'react';

const IconButton = ({icon, onClick, alt}) => (
  <div onClick={onClick} className="IconButton">
    <img src={icon} alt={alt} />
  </div>
)

export default IconButton