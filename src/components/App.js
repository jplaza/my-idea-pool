import React, { Component } from 'react';
import {
  Route,
  Redirect
} from 'react-router-dom'

import './App.css';

import SignupForm from '../containers/SignupForm';
import LoginForm from '../containers/LoginForm';
import IdeaPool from '../containers/IdeaPool';
import Sidebar from '../containers/Sidebar';


class Login extends Component {
  render() {
    if (this.props.isAuthenticated) {
      return <Redirect to={{pathname: "/"}} />
    }
    return (<LoginForm {...this.props} />)
  }
}

class Signup extends Component {
  render() {
    if (this.props.isAuthenticated) {
      return <Redirect to={{pathname: "/"}} />
    }
    return (<SignupForm {...this.props} />)
  }
}

const Index = (props) => (
  () => (
    <IdeaPool service={props.service} />
  )
)


const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      rest.isAuthenticated ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/login",
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

class App extends Component {
  render() {
    return (
      <div className="App">
        <Sidebar />
        <div className="App-main-container">
          <PrivateRoute
            exact
            path="/" {...this.props}
            component={Index(this.props)} />
          <Route
            path="/signup1"
            render={(props) => (
              <SignupForm {...this.props} />
            )} />
          <Route
            path="/signup"
            render={(props) => (
              <Signup {...this.props} />
            )} />
          <Route
            path="/login"
            render={(props) => (
              <Login {...this.props} />
            )} />
        </div>
      </div>
    );
  }
}

export default App;
