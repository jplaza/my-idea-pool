import React, { Component } from 'react';
import { Map } from 'immutable';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';

import logo from '../res/IdeaPool_icon@2x.png';


const Avatar = ({loggedIn, session, onLogoutClick}) => {
  const authUser = session.get('user') || Map({})
  return (
    <div className={((loggedIn && authUser)? "Avatar ":"hidden ")}>
      <div className="Avatar-divider" />
      <div className="Avatar-img">
        <img alt={authUser.get('name')} src={authUser.get('avatar_url')} />
      </div>
      <div className="Avatar-name">{authUser.get('name')}</div>
      <div className="Avatar-logout"
           onClick={e => onLogoutClick(session.get('token'))}>
        Log out
      </div>
    </div>
  )
}

class Sidebar extends Component {
  render() {
    return (
      <nav className="App-nav">
        <div className="App-logo">
          <img src={logo} alt="logo" />
        </div>
        <div className="App-name">The Idea Pool</div>
        <Avatar {...this.props} />
      </nav>
    )
  }
}

Sidebar.propTypes = {
  onLogoutClick: PropTypes.func.isRequired,
  session: ImmutablePropTypes.contains({
    token: ImmutablePropTypes.contains({
      jwt: PropTypes.string,
      refresh_token: PropTypes.string
    }),
    user: ImmutablePropTypes.contains({
      email: PropTypes.string,
      name: PropTypes.string,
      avatar_url: PropTypes.string
    })
  }).isRequired
}

export default Sidebar
