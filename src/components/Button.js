import React, { Component } from 'react';
import './Button.css';

class Button extends Component {
  render() {
    const btnClass = this.props.btnStyle ? this.props.btnStyle:'default'
    return (
      <button
          type="button"
          className={"Button " + btnClass}
          onClick={this.props.onClick}>
        { this.props.children }
      </button>
    );
  }
}

export default Button;
