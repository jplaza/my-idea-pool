import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import IconButton from './IconButton';
import TextField from './TextField';

import saveIcon from '../res/Confirm_V@2x.png';
import cancelIcon from '../res/Cancel_X@2x.png';


class EditIdea extends Component {
  constructor(props) {
    super(props)
    this.handleContentChange = this.handleContentChange.bind(this)
    this.handleImpactChange = this.handleImpactChange.bind(this)
    this.handleEaseChange = this.handleEaseChange.bind(this)
    this.handleConfidenceChange = this.handleConfidenceChange.bind(this)
  }
  handleContentChange(evt) {
    this.props.onIdeaChange({
      id: this.props.idea.get('id'),
      content: evt.target.value
    })
  }
  handleImpactChange(evt) {
    this.props.onIdeaChange({
      id: this.props.idea.get('id'),
      impact: parseInt(evt.target.value, 10)
    })
  }
  handleEaseChange(evt) {
    this.props.onIdeaChange({
      id: this.props.idea.get('id'),
      ease: parseInt(evt.target.value, 10)
    })
  }
  handleConfidenceChange(evt) {
    this.props.onIdeaChange({
      id: this.props.idea.get('id'),
      confidence: parseInt(evt.target.value, 10)
    })
  }
  render() {
    const props = this.props
    const msg = {
      text: props.idea.getIn(['validation', 'content']),
      type: "error"
    }
    return (
      <div className="Idea-container">
        <div className="Idea-container-content Idea-container-cell">
          <TextField
            placeholder=""
            value={props.idea.get('content')}
            onChange={this.handleContentChange}
            message={msg} />
        </div>
        <div className="Idea-container-score Idea-container-cell">
          <select onChange={this.handleImpactChange}
                  value={props.idea.get('impact')}>
            {
              props.scoreRange.map(s => <option key={s} value={s}>{s}</option>)
            }
          </select>
        </div>
        <div className="Idea-container-score Idea-container-cell">
          <select onChange={this.handleEaseChange}
                  value={props.idea.get('ease')}>
            {
              props.scoreRange.map(s => <option key={s} value={s}>{s}</option>)
            }
            </select>
        </div>
        <div className="Idea-container-score Idea-container-cell">
          <select onChange={this.handleConfidenceChange}
                  value={props.idea.get('confidence')}>
            {
              props.scoreRange.map(s => <option key={s} value={s}>{s}</option>)
            }
          </select>
        </div>
        <div className="Idea-container-score Idea-container-cell">
          {parseFloat(props.idea.get('average'), 10).toFixed(2)}
        </div>
        <div className="Idea-container-actions Idea-container-cell IdeaEdit">
          <IconButton onClick={e => props.onSaveClick(props.idea)}
                      icon={saveIcon}
                      alt="Save" />
          <IconButton onClick={e => props.onCancelClick(props.idea)}
                      icon={cancelIcon}
                      alt="Cancel" />
        </div>
      </div>
    )
  }
}

EditIdea.propTypes = {
  scoreRange: ImmutablePropTypes.seq,
  idea: ImmutablePropTypes.contains({
    content: PropTypes.string,
    average: PropTypes.number,
    impact: PropTypes.number,
    ease: PropTypes.number,
    confidence: PropTypes.number
  }),
  onSaveClick: PropTypes.func.isRequired,
  onCancelClick: PropTypes.func.isRequired,
  onIdeaChange: PropTypes.func.isRequired
}

export default EditIdea
