import React, { Component } from 'react';
import { Map } from 'immutable';
import './IdeaList.css';
import PropTypes from 'prop-types';
import Alert from './Alert';
import EditIdea from './EditIdea';
import Idea from './Idea';

import emptyImg from '../res/bulb@2x.png';
import addBtnImg from '../res/btn_addanidea@2x.png';
import './Dialog.css';


const EmptyList = (props) => (
  <div className="IdeaList-empty-msg">
    <div className="IdeaList-empty-msg-img">
      <img alt="Ideas" src={emptyImg} />
    </div>
    <div>Got ideas?</div>
  </div>
)

const AddIdeaButton = ({onAddClick}) => (
  <div className="IdeaList-AddIdeaButton">
    <div
      className="AddIdeaButton"
      onClick={onAddClick}
      style={{
        backgroundImage: `url(${addBtnImg})`
      }} />
  </div>
)

class Ideas extends Component {
  randID() {
    return Math.round(Math.random() * 1000000000000)
  }
  render() {
    const ideas = this.props.ideas
    return (
      <div className="Ideas-container">
        <div className="Idea-container Idea-List-header">
          <div className="Idea-container-content Idea-container-cell"></div>
          <div className="Idea-container-score Idea-container-cell">
            Impact
          </div>
          <div className="Idea-container-score Idea-container-cell">
            Ease
          </div>
          <div className="Idea-container-score Idea-container-cell">
            Confidence
          </div>
          <div className="Idea-container-score Idea-container-cell">
            <strong>Avg.</strong>
          </div>
          <div className="Idea-container-actions Idea-container-cell">
          </div>
        </div>
        {this.props.children}
        {
          ideas.sortBy(idea => -idea.get('average')).map((idea, id) => (
            idea.get('editting')? (
              <EditIdea key={(id || this.randID())}
                        idea={idea}
                        onSaveClick={this.props.onIdeaSaveClick}
                        onCancelClick={this.props.onIdeaEditCancelClick}
                        onIdeaChange={this.props.onIdeaChange}
                        scoreRange={this.props.scoreRange} />
            ):(
              <Idea key={(id || this.randID())}
                    idea={idea}
                    onEditClick={this.props.onIdeaEditClick}
                    onDeleteClick={this.props.onIdeaDeleteClick} />
            )
          )).valueSeq().toArray()
        }
      </div>
    )
  }
}

const AddIdea = (props) => {
  return props.idea? (
    <EditIdea {...props} />
  ):(
    <div />
  )
}

class DeleteConfirmDialog extends Component {
  render() {
    if (!this.props.ideaToDelete) {
      return <div />
    }
    return (
      <div className="Dialog-container">
        <div className="Dialog-backdrop"></div>
        <div className="Dialog">
          <div className="Dialog-title">
            Are you sure?
          </div>
          <div className="Dialog-message">
            This idea will be permanently deleted
          </div>
          <div className="Dialog-buttons">
            <div className="Dialog-button"
                 onClick={this.props.onCancelClick}>
              Cancel
            </div>
            <div className="Dialog-button Dialog-confirm-button"
                 onClick={this.props.onAcceptClick}>
              OK
            </div>
          </div>
        </div>
      </div>
    )
  }
}


class IdeaList extends Component {
  constructor(props) {
    super(props)
    this.handleIdeaListClick = this.handleIdeaListClick.bind(this)
  }
  handleIdeaListClick(evt) {
    this.props.onIdeaListClick(this.props.data)
  }
  componentDidMount() {
    this.props.fetchIdeas()
  }
  render() {
    const props = this.props
    const ideas = props.ideas || Map({})
    return (
      <div className="IdeaList">
        <header className="Section-header">
          <h1>My Ideas</h1>
          <AddIdeaButton onAddClick={this.props.onAddClick} />
        </header>
        <Alert type="error" message={props.error} />
        {
          ideas.merge(props.add).size? (
            <Ideas ideas={this.props.ideas}
                   onIdeaEditClick={this.props.onIdeaEditClick}
                   onIdeaEditCancelClick={this.props.onIdeaEditCancelClick}
                   onIdeaDeleteClick={this.props.onIdeaDeleteClick}
                   onIdeaSaveClick={this.props.onIdeaSaveClick}
                   onIdeaChange={props.onIdeaChange}
                   scoreRange={this.props.scoreRange}>
              <AddIdea
                idea={props.add}
                onSaveClick={props.onNewIdeaSave}
                onCancelClick={props.onNewIdeaCancel}
                onIdeaChange={props.onNewIdeaChange}
                scoreRange={props.scoreRange} />
            </Ideas>
          ):(
            !this.props.add && <EmptyList />
          )
        }
        <DeleteConfirmDialog
          ideaToDelete={props.delete}
          onCancelClick={e => props.onIdeaDeleteCancelClick()}
          onAcceptClick={e => props.onIdeaConfirmDeleteClick(props.delete)} />
      </div>
    );
  }
}

IdeaList.propTypes = {
  onAddClick: PropTypes.func.isRequired,
  onIdeaEditClick: PropTypes.func.isRequired,
  onIdeaDeleteClick: PropTypes.func.isRequired,
  onIdeaChange: PropTypes.func.isRequired,
  onIdeaSaveClick: PropTypes.func.isRequired,
  onIdeaEditCancelClick: PropTypes.func.isRequired,
  onNewIdeaSave: PropTypes.func.isRequired,
  onNewIdeaCancel: PropTypes.func.isRequired,
  onNewIdeaChange: PropTypes.func.isRequired
}

export default IdeaList;
