import React, { Component } from 'react';
import Button from './Button';
import TextField from './TextField';
import './Login.css';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import Alert from './Alert';
import { NavLink } from 'react-router-dom'

class Login extends Component {
  constructor(props) {
    super(props)
    this.handleLoginClick = this.handleLoginClick.bind(this)
  }
  handleLoginClick(evt) {
    this.props.onLoginClick(this.props.data)
  }
  buttonText() {
    return this.props.submitting? 'Working...':'Log in'
  }
  render() {
    const props = this.props
    const data = this.props.data
    const errorMessage = (msg) => ({
      text: msg,
      type: 'error'
    })
    return (
      <div className="Login">
        <h1>Log in</h1>
        <Alert type="error" message={props.errors.get('_error')} />
        <form>
          <TextField
            placeholder="Email"
            type="text"
            value={data.get('email') || ''}
            onChange={e => this.props.onEmailChange(e.target.value)}
            message={errorMessage(props.errors.get('email'))} />
          <TextField
            placeholder="Password"
            type="password"
            value={data.get('password') || ''}
            onChange={e => this.props.onPasswordChange(e.target.value)}
            message={errorMessage(props.errors.get('password'))} />
          <div className="Login-form-footer">
            <Button
                btnStyle="primary"
                disable={this.props.submitting}
                onClick={this.handleLoginClick}>
              { this.buttonText() }
            </Button>
            <div className="Login-signup-link">
              Don't have an account? <NavLink to="/signup">Create an account</NavLink>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

Login.propTypes = {
  onLoginClick: PropTypes.func.isRequired,
  onEmailChange: PropTypes.func.isRequired,
  onPasswordChange: PropTypes.func.isRequired,
  data: ImmutablePropTypes.contains({
    email: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired
  }).isRequired,
  errors: ImmutablePropTypes.contains({
    email: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired
  }).isRequired
}

export default Login;
