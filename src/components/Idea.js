import React, { Component } from 'react';
import IconButton from './IconButton';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';

import editIcon from '../res/pen@2x.png';
import deleteIcon from '../res/bin@2x.png';


class Idea extends Component {
  render() {
    const props = this.props
    return (
      <div className="Idea-container">
        <div className="Idea-container-content Idea-container-cell">
          {props.idea.get('content')}
        </div>
        <div className="Idea-container-score Idea-container-cell">
          {props.idea.get('impact')}
        </div>
        <div className="Idea-container-score Idea-container-cell">
          {props.idea.get('ease')}
        </div>
        <div className="Idea-container-score Idea-container-cell">
          {props.idea.get('confidence')}
        </div>
        <div className="Idea-container-score Idea-container-cell">
          {parseFloat(props.idea.get('average'), 10).toFixed(2)}
        </div>
        <div className="Idea-container-actions Idea-container-cell">
          <IconButton onClick={e => props.onEditClick(props.idea)}
                      icon={editIcon} alt="Edit" />
          <IconButton onClick={e => props.onDeleteClick(props.idea)}
                      icon={deleteIcon} alt="Delete" />
        </div>
      </div>
    )
  }
}

Idea.propTypes = {
  scoreRange: ImmutablePropTypes.seq,
  idea: ImmutablePropTypes.contains({
    content: PropTypes.string,
    average: PropTypes.number,
    impact: PropTypes.number,
    ease: PropTypes.number,
    confidence: PropTypes.number
  }),
  onEditClick: PropTypes.func.isRequired,
  onDeleteClick: PropTypes.func.isRequired
}

export default Idea
