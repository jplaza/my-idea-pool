import React, {Component} from 'react'
import './TextField.css'
import PropTypes from 'prop-types';

const FieldHelp = ({text, type}) => (
  <div className={(text? "TextField-help-text":"hidden") + " " + type}>
    {text}
  </div>
)

class TextField extends Component {
  render() {
    const props = this.props
    const message = props.message || {}
    return (
      <div className="TextField">
        <input
          placeholder={props.placeholder}
          type={props.type || "text"}
          value={props.value || ''}
          onChange={props.onChange} />
        <FieldHelp {...message} />
      </div>
    )
  }
}

TextField.propTypes = {
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  message: PropTypes.shape({
    text: PropTypes.string,
    type: PropTypes.oneOf(['error', 'success', 'warning', 'help']).isRequired
  })
}

export default TextField
